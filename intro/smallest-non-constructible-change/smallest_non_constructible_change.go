package smallest_non_constructible_change

import (
	"sort"
)

func smallest_non_constructible_change_value(change []int) int {
	sort.Ints(change)
	var max_value int
	for _, v := range change {
		if v > max_value+1 {
			break
		}
		max_value += v
	}
	return max_value + 1
}
