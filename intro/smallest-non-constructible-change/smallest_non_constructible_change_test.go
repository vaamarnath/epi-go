package smallest_non_constructible_change

import (
	"testing"
)

func TestGivenInputs(t *testing.T) {
	var testcases = []struct {
		change       []int
		expected_max int
	}{
		{[]int{1, 1, 1, 1, 1, 5, 10, 25}, 21},
		{[]int{12, 2, 1, 15, 2, 4}, 10},
	}

	for _, tc := range testcases {
		if m := smallest_non_constructible_change_value(tc.change); m != tc.expected_max {
			t.Errorf("exepected max value: %d, got %d\n", tc.expected_max, m)
		}
	}
}
